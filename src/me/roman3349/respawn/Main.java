package me.roman3349.respawn;

import java.util.logging.Logger;

import me.frostbitecz.frameworks.jcommands.CommandManager;

import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

    private static final Logger console = Logger.getLogger("Minecraft");

    @Override
    public void onEnable() {
        this.getServer().getPluginManager().registerEvents(new Events(this), this);
        CommandManager.registerCommands(this, new Commands(this));
        console.info("[Respawn] Plugin has been enabled!");
    }

    @Override
    public void onDisable() {
        this.saveConfig();
        console.info("[Respawn] Plugin has been disabled!");
    }

}
