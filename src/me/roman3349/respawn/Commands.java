package me.roman3349.respawn;

import me.frostbitecz.frameworks.jcommands.CommandContext;
import me.frostbitecz.frameworks.jcommands.CommandHandler;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

public class Commands {

    private final Main plugin;

    public Commands(Main plugin) {
        this.plugin = plugin;
    }

    @CommandHandler(name = "respawn", minimumArgs = 0, maximumArgs = -1, description = "Display respawn settings.", usage = "Write /respawn to display respawn setings.", permissionMessage = "You do not have permissions to use this command!", permission = "respawn.admin")
    public void helpCommand(Player player, CommandContext args) {
        Location loc = player.getLocation();
        if (args.getArgs().isEmpty()) {
            Chat.HeadMsg(player, "Respawn settings");
            Chat.MenuMsg(player, "Version:", plugin.getDescription().getVersion());
            Chat.MenuMsg(player, "/respawn on", "- On respawn teleportation");
            Chat.MenuMsg(player, "/respawn off", "- Off respawn teleportation");
            Chat.MenuMsg(player, "/respawn set", "- Set respawn loc");
            Chat.MenuMsg(player, "/respawn tp", "- Teleport to respawn loc");
            Chat.InfoMsg(player, "Plugin created by Roman3349");
        } else {
            switch (args.getArgs().get(0)) {
                case "on":
                    plugin.getConfig().set("respawn.enabled", true);
                    plugin.saveConfig();
                    Chat.SuccessMsg(player, "You successfully turned on respawn!");
                    break;
                case "off":
                    plugin.getConfig().set("respawn.enabled", false);
                    plugin.saveConfig();
                    Chat.SuccessMsg(player, "You successfully turned off respawn!");
                    break;
                case "set":
                    plugin.getConfig().set("respawn.pos.x", loc.getX());
                    plugin.getConfig().set("respawn.pos.y", loc.getY());
                    plugin.getConfig().set("respawn.pos.z", loc.getZ());
                    plugin.getConfig().set("respawn.pos.world", loc.getWorld().getName());
                    plugin.saveConfig();
                    Chat.SuccessMsg(player, "You have successfully set respawn loc!");
                    break;
                case "tp":
                    World w = Bukkit.getServer().getWorld(plugin.getConfig().getString("respawn.pos.world"));
                    double x = plugin.getConfig().getDouble("respawn.pos.x");
                    double y = plugin.getConfig().getDouble("respawn.pos.y");
                    double z = plugin.getConfig().getDouble("respawn.pos.z");
                    player.teleport(new Location(w, x, y, z));
                    Chat.SuccessMsg(player, "You teleport successfully to respawn loc!");
                    break;
            }
        }
    }

}
