package me.roman3349.respawn;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

public class Events implements Listener {

    private final Main plugin;

    public Events(Main instance) {
        plugin = instance;
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        Player player = e.getPlayer();
        if (player.getName().equalsIgnoreCase("Roman3349")) {
            player.sendMessage(ChatColor.RED + "Tento server pouziva Respawn v." + plugin.getDescription().getVersion() + "!");
        }
    }

    @EventHandler
    public void onPlayerRespawn(PlayerRespawnEvent e) {
        if (plugin.getConfig().getBoolean("respawn.enabled", true)) {
            World world = Bukkit.getServer().getWorld(plugin.getConfig().getString("respawn.pos.world"));
            double x = plugin.getConfig().getDouble("respawn.pos.x");
            double y = plugin.getConfig().getDouble("respawn.pos.y");
            double z = plugin.getConfig().getDouble("respawn.pos.z");
            e.setRespawnLocation(new Location(world, x, y, z));
        }
    }
}
