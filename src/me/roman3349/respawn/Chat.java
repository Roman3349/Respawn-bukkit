package me.roman3349.respawn;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public class Chat {

    public static void SuccessMsg(CommandSender sender, String message) {
        sender.sendMessage(ChatColor.GREEN + message);
    }

    public static void ErrorMsg(CommandSender sender, String message) {
        sender.sendMessage(ChatColor.RED + message);
    }

    public static void InfoMsg(CommandSender sender, String message) {
        sender.sendMessage(ChatColor.YELLOW + message);
    }

    public static void MenuMsg(CommandSender sender, String cmd, String description) {
        sender.sendMessage(ChatColor.YELLOW + cmd + ChatColor.RESET + " " + description);
    }

    public static void HeadMsg(CommandSender sender, String name) {
        sender.sendMessage(ChatColor.GRAY + "******* " + ChatColor.YELLOW + name + ChatColor.GRAY + " *******");
    }

}
